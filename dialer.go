package natsdialer

import (
	"errors"

	"github.com/nats-io/nats.go"
)

// NewNatsConnection - creates a new nats connection with the provided options.
func NewNatsConnection(natsURL string, connectionOptions []nats.Option) (*nats.Conn, error) {
	natsConnection, err := nats.Connect(natsURL, connectionOptions...)
	if err != nil {
		return nil, err
	}

	return natsConnection, nil
}

// NewJetStreamConnection - creates a new jetstream connection with the provided options.
func NewJetStreamConnection(
	natsURL string,
	connectionOptions []nats.Option,
	jetStreamOptions []nats.JSOpt,
) (*nats.Conn, nats.JetStreamContext, error) {
	natsConnection, err := NewNatsConnection(natsURL, connectionOptions)
	if err != nil {
		return nil, nil, err
	}

	// Create JetStream Context
	natsJetStream, err := natsConnection.JetStream(jetStreamOptions...)
	if err != nil {
		return nil, nil, err
	}

	return natsConnection, natsJetStream, nil
}

// CreateStreamWithSubject - creates a new connection with the provided options and also creates the stream.
func CreateStreamWithSubject(
	natsURL string,
	connectionOptions []nats.Option,
	jetStreamOptions []nats.JSOpt,
	streamConfig *nats.StreamConfig,
) (*nats.Conn, nats.JetStreamContext, error) {
	natsConnection, natsJetStream, err := NewJetStreamConnection(
		natsURL,
		connectionOptions,
		jetStreamOptions,
	)
	if err != nil {
		return nil, nil, err
	}

	// Streams need to be created before publishing.
	if streamConfig != nil {
		if streamConfig.MaxAge == 0 {
			return nil, nil, errors.New("MaxAge needs to be specified for a stream")
		}
		_, err = natsJetStream.AddStream(streamConfig)
		if err != nil {
			return nil, nil, err
		}
	}

	return natsConnection, natsJetStream, nil
}
