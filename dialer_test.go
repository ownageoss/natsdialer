package natsdialer_test

import (
	"testing"
	"time"

	"gitlab.com/ownageoss/natsdialer"

	"github.com/nats-io/nats.go"
)

func TestNewNatsConnection(t *testing.T) {
	t.Parallel()

	_, err := natsdialer.NewNatsConnection("nats://localhost:4222", nil)
	if err != nil {
		t.Fatal("expected no errors")
	}
}

func TestNewJetStreamConnection(t *testing.T) {
	t.Parallel()

	_, _, err := natsdialer.NewJetStreamConnection("nats://localhost:4222",
		nil,
		nil,
	)
	if err != nil {
		t.Fatal("expected no errors")
	}
}

func TestCreateStreamWithSubject(t *testing.T) {
	t.Parallel()

	streamName := "tester"

	_, js, err := natsdialer.CreateStreamWithSubject("nats://localhost:4222",
		nil,
		nil,
		&nats.StreamConfig{
			Name:   streamName,
			MaxAge: time.Duration(1) * time.Minute,
		},
	)
	if err != nil {
		t.Fatal("expected no errors")
	}

	// Cleanup
	_ = js.DeleteStream(streamName)
}
