PACKAGE_NAME=natsdialer

updatedeps:
	go get -u ./...
	go mod tidy

sbom:
	bom generate --name gitlab.com/ownageoss/$(PACKAGE_NAME) --output=$(PACKAGE_NAME).spdx .
	bom document outline $(PACKAGE_NAME).spdx

checks:
	gitleaks detect -v --no-git
	golangci-lint run --no-config
	govulncheck ./...
	gosec -tests ./...
	go install github.com/nats-io/nats-server/v2@latest
	nats-server -js & sleep 2 && go test -race -v ./...

brutal:
	golangci-lint run --enable-all