# NATS dialer <!-- omit in toc -->

[NATS](nats.io) is a high performance, distributed and scalable messaging system.

This repo contains a simple golang dialer that wraps common connection patterns. The main intention is to standardize connections to NATS.

- [NATS Overview](#nats-overview)
- [Streams](#streams)
- [Subjects](#subjects)
- [Publishers](#publishers)
- [Subscribers](#subscribers)
- [Example Usage](#example-usage)
- [Versioning](#versioning)
- [Project status](#project-status)
- [Supported Go versions](#supported-go-versions)
- [Software Bill of Materials (SBOM)](#software-bill-of-materials-sbom)

## NATS Overview

It is highly recommended that you go through the official [Overview](https://docs.nats.io/nats-concepts/overview) and familiarize yourself with `NATS`.

## Streams

Streams provided persistence to messages.

Streams need to be created before messages can be sent via the stream.

Here are a few key points around streams:

- A stream needs to be created with at least 1 subject, example: `stream1` with `subject1`
- Streams can have more than 1 subject, example, `stream1` with subjects `subject1`, `subject2`, etc.
- Subjects are unique system wide, i.e. `subject1` cannot belong to both `stream1` and `stream2`.
- If a stream needs to be updated with more subjects, all previous subjects needs to be provided during the update. Otherwise, the previous subjects will be dropped from the stream.

To keep things secure and simple, we recommend using the following convention:

- Restrict the scope of a stream to a single project.
- Create the stream with a wildcard subject, example: stream `project1` with wildcard subject `project1.*`
- The dialer does not offer capabilities to update a stream. This is to ensure that subjects are not accidentally modified when a stream is updated.

## Subjects

Subjects are unique system wide.

Subjects may or may not belong to a stream.

Subjects that belong to a stream, can be consumed without persistence.

## Publishers

Publishers publish messages to subjects.

Messages can be published with or without persistence.

## Subscribers

Subscribers consume messages from subjects.

Messages can be consumed with or without persistence.

## Example Usage

The [examples](./examples) folder has a basic [publisher](./examples/publisher/) and [subscriber](./examples/subscriber/) provided for reference.

## Versioning

This project uses [Semantic Versioning 2.0.0](https://semver.org/).

## Project status

This library is used in production environments and is actively maintained.

## Supported Go versions

Go version support is aligned with the Go's [release policy](https://go.dev/doc/devel/release#policy).

## Software Bill of Materials (SBOM)

```
               _      
 ___ _ __   __| |_  __
/ __| '_ \ / _` \ \/ /
\__ \ |_) | (_| |>  < 
|___/ .__/ \__,_/_/\_\
    |_|               

 📂 SPDX Document gitlab.com/ownageoss/natsdialer
  │ 
  │ 📦 DESCRIBES 1 Packages
  │ 
  ├ natsdialer
  │  │ 🔗 20 Relationships
  │  ├ CONTAINS FILE Makefile (Makefile)
  │  ├ CONTAINS FILE README.md (README.md)
  │  ├ CONTAINS FILE dialer.go (dialer.go)
  │  ├ CONTAINS FILE dialer_test.go (dialer_test.go)
  │  ├ CONTAINS FILE .gitignore (.gitignore)
  │  ├ CONTAINS FILE .gitlab-ci.yml (.gitlab-ci.yml)
  │  ├ CONTAINS FILE LICENSE (LICENSE)
  │  ├ CONTAINS FILE examples/publisher/main.go (examples/publisher/main.go)
  │  ├ CONTAINS FILE examples/README.md (examples/README.md)
  │  ├ CONTAINS FILE examples/subscriber/main.go (examples/subscriber/main.go)
  │  ├ CONTAINS FILE go.sum (go.sum)
  │  ├ CONTAINS FILE go.mod (go.mod)
  │  ├ CONTAINS FILE natsdialer.png (natsdialer.png)
  │  ├ CONTAINS FILE natsdialer.spdx (natsdialer.spdx)
  │  ├ DEPENDS_ON PACKAGE golang.org/x/crypto@v0.36.0
  │  ├ DEPENDS_ON PACKAGE golang.org/x/sys@v0.31.0
  │  ├ DEPENDS_ON PACKAGE github.com/nats-io/nkeys@v0.4.10
  │  ├ DEPENDS_ON PACKAGE github.com/nats-io/nuid@v1.0.1
  │  ├ DEPENDS_ON PACKAGE github.com/klauspost/compress@v1.18.0
  │  └ DEPENDS_ON PACKAGE github.com/nats-io/nats.go@v1.39.1
  │ 
  └ 📄 DESCRIBES 0 Files
```