package main

import (
	"fmt"
	"log"
	"time"

	"gitlab.com/ownageoss/natsdialer"

	"github.com/nats-io/nats.go"
)

func main() {
	natsURL := "nats://localhost:4222"

	streamName := "project1"
	streamSubject := "subject1"

	connectionOptions := []nats.Option{
		// Activate retry logic if a connection cannot be established at the start.
		nats.RetryOnFailedConnect(true),
		nats.ReconnectHandler(func(_ *nats.Conn) {
			// Note that this will be invoked for the first asynchronous connect.
			log.Println("Connection to NATS restored ...")
		}),
	}

	natsConnection, natsJetStream, err := natsdialer.CreateStreamWithSubject(
		natsURL,
		connectionOptions,
		nil,
		&nats.StreamConfig{
			Name:     streamName,
			Subjects: []string{streamName + ".*"},
		},
	)
	if err != nil {
		log.Fatal(err)
	}

	log.Println("Connected as publisher...")
	log.Printf(
		"Publishing to %v.%v ...",
		streamName, streamSubject,
	)

	for i := 0; i < 10; i++ {
		_, err = natsJetStream.Publish(
			streamName+"."+streamSubject,
			[]byte(fmt.Sprintf("Message %v", i)),
		)
		if err != nil {
			log.Println(err)
		}

		time.Sleep(time.Second)
	}

	natsConnection.Close()
}
