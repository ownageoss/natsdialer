# Examples

First install `nats-server` as follows:

```sh
go install github.com/nats-io/nats-server/v2@latest
```

Then, run with `jetstream` (persistence) as follows:

```sh
nats-server -js
```

Basic publisher and subscriber programs are provided for reference.

```sh
# Run publisher first.
cd publisher
go run .

# Run subscriber in a new terminal.
cd subscriber
go run .
```