package main

import (
	"context"
	"log"

	"gitlab.com/ownageoss/natsdialer"

	"github.com/nats-io/nats.go"
)

func main() {
	ctx := context.Background()

	natsURL := "nats://localhost:4222"

	streamName := "project1"
	streamSubject := "subject1"

	connectionOptions := []nats.Option{
		// Activate retry logic if a connection cannot be established at the start.
		nats.RetryOnFailedConnect(true),
		nats.ReconnectHandler(func(_ *nats.Conn) {
			// Note that this will be invoked for the first asynchronous connect.
			log.Println("Connection to NATS restored ...")
		}),
	}

	natsConnection, natsJetStream, err := natsdialer.NewJetStreamConnection(
		natsURL,
		connectionOptions,
		nil,
	)
	if err != nil {
		log.Fatal(err)
	}

	log.Println("Connected as subscriber...")
	log.Printf(
		"Listenting for messages on %v.%v ...",
		streamName, streamSubject,
	)

	// Simple Async Subscriber
	go func() {
		_, _ = natsJetStream.Subscribe(
			streamName+"."+streamSubject,
			func(message *nats.Msg) {
				log.Println(string(message.Data))
				_ = message.Ack()
			},
			nats.Durable("test-durable"),
			nats.ManualAck(),
			nats.MaxAckPending(1),
		)
	}()

	// Block until we receive our signal.
	<-ctx.Done()

	natsConnection.Close()
}
